package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sort"
	"strings"
	"sync"
	"testing"
	"time"
)

const token = "myBestTokenEver"

type xmlUser struct {
	Id        int    `xml:"id"`
	FirstName string `xml:"first_name"`
	LastName  string `xml:"last_name"`
	Name      string `xml:"-"`
	Age       int    `xml:"age"`
	About     string `xml:"about"`
	Gender    string `xml:"gender"`
}

type searchServer struct {
	version    string `xml:"version,attr"`
	data       []byte
	List       []xmlUser `xml:"row"`
	query      string
	orderField string
	orderBy    int
	limit      int
	offset     int
}

func (ss *searchServer) read() error {
	xmlData, err := ioutil.ReadFile("dataset.xml") // the file is inside the local directory
	if err != nil {
		fmt.Printf("error: %v", err)
		return err
	}

	err = xml.Unmarshal(xmlData, &ss)
	if err != nil {
		fmt.Printf("error: %v", err)
		return err
	}

	resCh := func() chan []xmlUser {
		ch := make(chan []xmlUser, 1)
		go func() {
			users := make([]xmlUser, 0, len(ss.List))
			for _, val := range ss.List {
				val.Name = val.FirstName + val.LastName
				users = append(users, val)
			}
			ch <- users
			close(ch)
		}()
		return ch
	}
	ss.List = <-resCh()

	return nil
}

func (ss *searchServer) search() {
	if ss.query == "" {
		return
	}
	found := make([]xmlUser, 0, len(ss.List))
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		mutex := sync.Mutex{}
		for _, user := range ss.List {
			if strings.Contains(user.Name, ss.query) || strings.Contains(user.About, ss.query) {
				mutex.Lock()
				found = append(found, user)
				mutex.Unlock()
			}
		}
	}()
	wg.Wait()
	ss.List = found
}

func (ss *searchServer) sort() error {
	if ss.orderBy == OrderByAsIs {
		return nil
	}

	var sortFunc func(i, j int) bool
	switch ss.orderField {
	case "Id":
		if ss.orderBy == OrderByAsc {
			sortFunc = func(i, j int) bool { return ss.List[i].Id < ss.List[j].Id }
		} else {
			sortFunc = func(i, j int) bool { return ss.List[i].Id > ss.List[j].Id }
		}
	case "Age":
		if ss.orderBy == OrderByAsc {
			sortFunc = func(i, j int) bool { return ss.List[i].Age < ss.List[j].Age }
		} else {
			sortFunc = func(i, j int) bool { return ss.List[i].Age > ss.List[j].Age }
		}
	case "Name", "":
		if ss.orderBy == OrderByAsc {
			sortFunc = func(i, j int) bool { return ss.List[i].Name < ss.List[j].Name }
		} else {
			sortFunc = func(i, j int) bool { return ss.List[i].Name > ss.List[j].Name }
		}
	default:
		return fmt.Errorf(ErrorBadOrderField)
	}

	sort.Slice(ss.List, sortFunc)
	return nil
}

func (ss *searchServer) limits() {
	if ss.limit > 25 || ss.limit == 0 {
		ss.limit = 25
	}
	if ss.limit > len(ss.List) {
		ss.limit = len(ss.List)
	}
	since := ss.offset * ss.limit
	until := since + ss.limit
	ss.List = ss.List[since:until]
}

func (ss *searchServer) makeData() {
	if ss.limit < 0 {
		err := fmt.Errorf("limit must be > 0")
		fmt.Printf("error: %v", err)
		return
	}
	if ss.offset < 0 {
		err := fmt.Errorf("offset must be > 0")
		fmt.Printf("error: %v", err)
		return
	}
	err := ss.read()
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	ss.search()
	err = ss.sort()
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	ss.limits()

	ss.data, err = json.Marshal(ss.List)
	if err != nil {
		fmt.Println("error:", err)
		return
	}
}

func (ss *searchServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if ss.limit < 0 {
		err := fmt.Errorf("limit must be > 0")
		fmt.Printf("error: %v", err)
		return
	}
	if ss.offset < 0 {
		err := fmt.Errorf("offset must be > 0")
		fmt.Printf("error: %v", err)
		return
	}
	err := ss.read()
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	ss.search()
	err = ss.sort()
	if err != nil {
		fmt.Printf("error: %v", err)
		return
	}
	ss.limits()

	b, err := json.Marshal(ss.List)
	if err != nil {
		fmt.Println("error:", err)
		return
	}

	fmt.Fprintf(w, "raw body: %s", string(b))
}

// ------------------------------------------------------------------------------------------------------------------ //
type findUsersResult struct {
	Users searchServer
	Err   string
}

type TestCase struct {
	Request SearchRequest
	Result  *findUsersResult
	IsError bool
}

func TestFindUsers(t *testing.T) {
	cases := []TestCase{
		{
			Request: SearchRequest{
				OrderField: "__broken_json",
			},
			Result:  nil,
			IsError: true,
		},
		{
			Request: SearchRequest{
				OrderField: "__limit_1",
			},
			Result:  nil,
			IsError: false,
		},
		{
			Request: SearchRequest{
				OrderField: "__limit_42",
				Limit:      42,
			},
			Result:  nil,
			IsError: false,
		},
	}
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		key := r.FormValue("order_field")
		switch key {
		case "__broken_json":
			w.WriteHeader(http.StatusOK)
			io.WriteString(w, `{"status": 400`) //broken json
		case "__limit_1":
			w.WriteHeader(http.StatusOK)
			ss := searchServer{
				query: "Wolf",
			}
			ss.makeData()
			io.WriteString(w, string(ss.data))
		case "__limit_42":
			w.WriteHeader(http.StatusOK)
			ss := searchServer{
				query: "Wolf",
			}
			ss.makeData()
			io.WriteString(w, string(ss.data))
		default:
			w.WriteHeader(http.StatusInternalServerError)
		}
	}))
	sc := SearchClient{
		AccessToken: "mybesttokerever",
		URL:         ts.URL,
	}

	for caseNum, item := range cases {
		_, err := sc.FindUsers(item.Request)

		if err != nil && !item.IsError {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
		if err == nil && item.IsError {
			t.Errorf("[%d] expected error, got nil", caseNum)
		}
	}
	ts.Close()
}

func TestOffset(t *testing.T) {
	offset := TestCase{
		Request: SearchRequest{
			Offset: -1,
		},
		Result:  nil,
		IsError: true,
	}
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		key := r.FormValue("offset")
		switch key {
		case "-1":
			w.WriteHeader(http.StatusOK)
		default:
			w.WriteHeader(http.StatusInternalServerError)
		}
	}))
	sc := SearchClient{
		AccessToken: token,
		URL:         ts.URL,
	}
	_, err := sc.FindUsers(offset.Request)
	if err != nil && !offset.IsError {
		t.Errorf("unexpected offset error check: %#v", err)
	}
	ts.Close()
}

func TestLimit(t *testing.T) {
	cases := []TestCase{
		{
			Request: SearchRequest{
				Limit: -1,
			},
			Result:  nil,
			IsError: true,
		},
		{
			Request: SearchRequest{
				Limit: 42,
			},
			Result:  nil,
			IsError: true,
		},
	}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		key := r.FormValue("limit")
		switch key {
		case "-1":
			w.WriteHeader(http.StatusOK)
		case "26":
			w.WriteHeader(http.StatusOK)
		default:
			w.WriteHeader(http.StatusInternalServerError)
		}
	}))
	sc := SearchClient{
		AccessToken: token,
		URL:         ts.URL,
	}

	for caseNum, item := range cases {
		_, err := sc.FindUsers(item.Request)

		if err != nil && !item.IsError {
			t.Errorf("[%d] unexpected limit error: %#v", caseNum, err)
		}
	}
	ts.Close()
}

func TestStatusCodes(t *testing.T) {
	cases := []TestCase{
		{
			Request: SearchRequest{
				OrderField: "__bad_access_token",
			},
			Result:  nil,
			IsError: true,
		},
		{
			Request: SearchRequest{
				OrderField: "__bad_request_bad_error",
			},
			Result:  nil,
			IsError: true,
		},
		{
			Request: SearchRequest{
				OrderField: "__bad_request_known_error",
			},
			Result:  nil,
			IsError: true,
		},
		{
			Request: SearchRequest{
				OrderField: "__bad_request_unknown_error",
			},
			Result:  nil,
			IsError: true,
		},
		{
			Request: SearchRequest{
				OrderField: "__internal_error",
			},
			Result:  nil,
			IsError: true,
		},
	}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		key := r.FormValue("order_field")
		switch key {
		case "__bad_access_token":
			w.WriteHeader(http.StatusUnauthorized)
		case "__bad_request_bad_error":
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`bad error`))
		case "__bad_request_known_error":
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"Error": "ErrorBadOrderField"}`))
		case "__bad_request_unknown_error":
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"Error": "UnknownError"}`))
		default:
			w.WriteHeader(http.StatusInternalServerError)
		}
	}))
	sc := SearchClient{
		AccessToken: token,
		URL:         ts.URL,
	}

	for caseNum, item := range cases {
		_, err := sc.FindUsers(item.Request)

		if err != nil && !item.IsError {
			t.Errorf("[%d] unexpected error: %#v", caseNum, err)
		}
	}
	ts.Close()
}

func TestTimeout(t *testing.T) {
	tc := TestCase{
		Request: SearchRequest{},
		Result:  nil,
		IsError: true,
	}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(time.Second * 2)
	}))

	sc := SearchClient{
		AccessToken: token,
		URL:         ts.URL,
	}

	_, err := sc.FindUsers(tc.Request)
	if err != nil && !tc.IsError {
		t.Errorf("unexpected error: %#v", err)
	}

	ts.Close()
}

func TestUnknownError(t *testing.T) {
	tc := TestCase{
		Request: SearchRequest{},
		Result:  nil,
		IsError: true,
	}

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(520)
	}))

	sc := SearchClient{}

	_, err := sc.FindUsers(tc.Request)
	if err != nil && !tc.IsError {
		t.Errorf("unexpected error: %#v", err)
	}

	ts.Close()
}
